from django.forms import ModelForm
from cars.models import URL

class URLForm(ModelForm):
    class Meta:
        model = URL
        fields = '__all__'