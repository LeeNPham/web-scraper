#import requests to use get on the site
import requests
#import BeautifulSoup
from bs4 import BeautifulSoup
# import pandas as pd

#get the first page (list view)

def get_data(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, "lxml")


    cars = soup.find_all('li', class_='result-row')

    data = []

    for car in cars:
        cars = {}
        cars["Price"] = car.find('span', class_='result-price').text.strip()
        try:
            cars["URL"] = car.find('a', class_='result-image gallery')['href']
            # cars["Image"] = car.find('img')['src']
        except:
            print('No IMAGE!')
        cars["Title"] = car.find('a', class_='result-title hdrlnk').text.strip()
        data.append(cars)
    return data